package br.com.itau.agenda.console;

import java.util.Scanner;

import br.com.itau.agenda.objetos.Agenda;

public class Console {

	private static Scanner scanner = new Scanner(System.in);
	
	public static void imprimeSaudacao() {
		System.out.println("Olá, seja bem vindo a nosso sistema de agenda!");
		System.out.println("Por favor insira seu nome para continuar ");
	}
	
	public static void imprimePerguntaNomeContato() {
		System.out.println("Qual o nome do contato que você deseja adicionar?");
	}
	
	public static void imprimePerguntaTelefoneContato() {
		System.out.println("Qual o telefone desse contato");
	}
	
	public static void imprimePerguntaNovoContato() {
		System.out.println("Deseja criar um novo contato? (Sim/Não)");
	}
	
	public static void imprimeAgenda(Agenda agenda) {
		System.out.println(agenda);
	}
	
	public static String lerString() {
		String texto = scanner.nextLine();
		return texto;
	}
	
	public static long lerLong() {
		String entrada = scanner.nextLine();
		
		Long valor = Long.parseLong(entrada);
		
		return valor;
	}
	
	public static boolean lerSimOuNao() {
		String entrada = scanner.nextLine();
		
		boolean simOuNao = false;
		
		if(entrada.equalsIgnoreCase("Sim")) {
			simOuNao = true;
		}
		
		return simOuNao;
	}
	
}
